import Vue from "vue";
import Router from "vue-router";
import EmptyRoute from "./views/EmptyRoute.vue";
import Landing from "./views/Landing.vue";
// import Test from "./views/Test.vue";
import store from "./store";
import Login from "./views/Login.vue";
import StudentProfile from "./views/Student/StudentProfile.vue";
import StudentShow from "./views/Student/StudentShow.vue";
import StudentList from "./views/Student/StudentList.vue";
import StudentCreate from "./views/Student/StudentCreate.vue";
import StudentEdit from "./views/Student/StudentEdit.vue";
import FacultyProfile from "./views/Faculty/FacultyProfile.vue";
import FacultyShow from "./views/Faculty/FacultyShow.vue";
import FacultyList from "./views/Faculty/FacultyList.vue";
import FacultyCreate from "./views/Faculty/FacultyCreate.vue";
import FacultyEdit from "./views/Faculty/FacultyEdit.vue";
import StaffProfile from "./views/Staff/StaffProfile.vue";
import StaffShow from "./views/Staff/StaffShow.vue";
import StaffList from "./views/Staff/StaffList.vue";
import StaffCreate from "./views/Staff/StaffCreate.vue";
import StaffEdit from "./views/Staff/StaffEdit.vue";
import ClassList from "./views/Class/ClassList.vue";
import ClassShow from "./views/Class/ClassShow.vue";
import SemesterList from "./views/Semester/SemesterList.vue";
import SemesterShow from "./views/Semester/SemesterShow.vue";
import SubjectList from "./views/Subject/SubjectList.vue";
import SubjectShow from "./views/Subject/SubjectShow.vue";
import CurriculumList from "./views/Curriculum/CurriculumList.vue";
import CurriculumShow from "./views/Curriculum/CurriculumShow.vue";
import CurriculumCreate from "./views/Curriculum/CurriculumCreate.vue";
import CurriculumEdit from "./views/Curriculum/CurriculumEdit.vue";
import SectionList from "./views/Section/SectionList.vue";
import SectionShow from "./views/Section/SectionShow.vue";
import ScheduleBuilder from "./views/Schedule/ScheduleBuilder.vue";
import Home from "./views/Homepage.vue";
// import TestView from "./views/TestView.vue";
import ScheduleResult from "./views/Schedule/ScheduleResult.vue";
// import TestUpload from "./views/TestUpload.vue";
import NewsList from "./views/News/NewsList.vue";
import NewsCreate from "./views/News/NewsCreate.vue";
import NewsEdit from "./views/News/NewsEdit.vue";
import NewsShow from "./views/News/NewsShow.vue";
import NewsPublic from "./views/News/NewsPublic.vue";
import NewsPublicShow from "./views/News/NewsPublicShow.vue";
import page404 from "./views/404.vue";

Vue.use(Router);

const routes = [
  {
    path: "home",
    component: Home
  },
  {
    path: "student",
    component: EmptyRoute,
    children: [
      {
        path: "",
        component: StudentList,
        props: { type: `student` }
      },
      {
        path: "create",
        component: StudentCreate,
        beforeEnter(to, from, next) {
          if (store.state.user.role === `Registrar`) {
            next();
          } else {
            router.replace(`/${store.state.userGroup}/student`);
          }
        }
      },
      {
        path: ":id",
        component: EmptyRoute,
        children: [
          {
            path: "",
            component: StudentShow,
            props: {
              type: `student`
            }
          },
          {
            path: "edit",
            component: StudentEdit,
            beforeEnter(to, from, next) {
              if (store.state.user.role === `Registrar`) {
                next();
              } else {
                router.replace(`/${store.state.userGroup}/student`);
              }
            }
          }
        ]
      }
    ]
  },
  {
    path: "faculty",
    component: EmptyRoute,
    children: [
      {
        path: "",
        component: FacultyList,
        props: { type: `faculty` }
      },
      {
        path: "create",
        component: FacultyCreate,
        beforeEnter(to, from, next) {
          if (store.state.user.role === `Program Head`) {
            next();
          } else {
            router.replace(`/${store.state.userGroup}/faculty`);
          }
        }
      },
      {
        path: ":id",
        component: EmptyRoute,
        children: [
          {
            path: "",
            component: FacultyShow,
            props: { type: `faculty` }
          },
          {
            path: "edit",
            component: FacultyEdit,
            beforeEnter(to, from, next) {
              if (store.state.user.role === `Program Head`) {
                next();
              } else {
                router.replace(`/${store.state.userGroup}/faculty`);
              }
            }
          }
        ]
      }
    ]
  },
  {
    path: "staff",
    component: EmptyRoute,
    children: [
      {
        path: "",
        component: StaffList,
        props: { type: `staff` }
      },
      {
        path: "create",
        component: StaffCreate,
        beforeEnter(to, from, next) {
          if (store.state.user.role === `School Director`) {
            next();
          } else {
            router.replace(`/${store.state.userGroup}/staff`);
          }
        }
      },
      {
        path: ":id",
        component: EmptyRoute,
        children: [
          {
            path: "",
            component: StaffShow,
            props: {
              type: `staff`
            }
          },
          {
            path: "edit",
            component: StaffEdit,
            beforeEnter(to, from, next) {
              if (store.state.user.role === `School Director`) {
                next();
              } else {
                router.replace(`/${store.state.userGroup}/staff`);
              }
            }
          }
        ]
      }
    ]
  },
  {
    path: "class",
    component: EmptyRoute,
    children: [
      {
        path: "",
        component: ClassList,
        props: {
          type: `class`
        }
      },
      {
        path: ":id",
        component: ClassShow,
        props: {
          type: `class`
        }
      }
    ]
  },
  {
    path: "news",
    component: EmptyRoute,
    children: [
      {
        path: "",
        component: NewsList,
        props: {
          type: `news`
        }
      },
      {
        path: "create",
        component: NewsCreate
      },
      {
        path: ":id",
        component: EmptyRoute,
        children: [
          {
            path: "",
            component: NewsShow,
            props: {
              type: `news`
            }
          },
          {
            path: "edit",
            component: NewsEdit,
            props: {
              type: `news`
            }
          }
        ]
      }
    ]
  },
  {
    path: "section",
    component: EmptyRoute,
    children: [
      {
        path: "",
        component: SectionList,
        props: {
          type: `section`
        }
      },
      {
        path: ":id",
        component: SectionShow,
        props: {
          type: `section`
        }
      }
    ]
  },
  {
    path: "semester",
    component: EmptyRoute,
    children: [
      {
        path: "",
        component: SemesterList,
        props: {
          type: `semester`
        }
      },
      {
        path: ":id",
        component: SemesterShow,
        props: {
          type: `semester`
        }
      }
    ]
  },
  {
    path: "subject",
    component: EmptyRoute,
    children: [
      {
        path: "",
        component: SubjectList,
        props: {
          type: `subject`
        }
      },
      {
        path: ":id",
        component: SubjectShow,
        props: {
          type: `subject`
        }
      }
    ]
  },
  {
    path: "curriculum",
    component: EmptyRoute,
    children: [
      {
        path: "",
        component: CurriculumList,
        props: {
          type: `curriculum`
        }
      },
      {
        path: "create",
        component: CurriculumCreate
      },
      {
        path: ":id",
        component: EmptyRoute,
        children: [
          {
            path: "",
            component: CurriculumShow,
            props: {
              type: `curriculum`
            }
          },
          {
            path: "edit",
            component: CurriculumEdit,
            props: {
              type: `curriculum`
            }
          }
        ]
      }
    ]
  }
];
const beforeEnterProfile = function(to, from, next) {
  Vue.axios
    .get(
      `${store.state.url}/${store.state.userGroup}/api/${
        store.state.userGroup
      }/${store.state.user._id}`,
      { withCredentials: true }
    )
    .then(response => {
      store.dispatch(`login`, {
        user: response.data,
        userGroup: store.state.userGroup
      });
      next();
    });
};

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: EmptyRoute,
      children: [
        {
          path: "",
          component: Landing
        },
        {
          path: "news",
          component: EmptyRoute,
          children: [
            {
              path: "",
              component: NewsPublic,
              props: {
                type: `news`
              }
            },
            {
              path: ":id",
              component: NewsPublicShow,
              props: {
                type: `news`
              }
            }
          ]
        },
        {
          path: "login",
          component: EmptyRoute,
          children: [
            {
              path: "student",
              component: Login,
              props: { userGroup: `student` }
            },
            {
              path: "faculty",
              component: Login,
              props: { userGroup: `faculty` }
            },
            {
              path: "staff",
              component: Login,
              props: { userGroup: `staff` }
            }
          ]
        },
        {
          path: "student",
          component: EmptyRoute,
          meta: { requiresAuth: true },
          children: [
            {
              path: `profile`,
              component: EmptyRoute,
              children: [
                {
                  path: "",
                  component: StudentProfile,
                  beforeEnter: beforeEnterProfile
                },
                {
                  path: "edit",
                  component: StudentEdit
                }
              ]
            },
            ...routes
          ]
        },
        {
          path: "faculty",
          component: EmptyRoute,
          meta: { requiresAuth: true },
          children: [
            {
              path: `profile`,
              component: EmptyRoute,
              children: [
                {
                  path: "",
                  component: FacultyProfile,
                  beforeEnter: beforeEnterProfile
                },
                {
                  path: "edit",
                  component: FacultyEdit
                }
              ]
            },
            ...routes
          ]
        },
        {
          path: "staff",
          component: EmptyRoute,
          meta: { requiresAuth: true },
          children: [
            {
              path: `profile`,
              component: EmptyRoute,
              children: [
                {
                  path: "",
                  component: StaffProfile,
                  beforeEnter: beforeEnterProfile
                },
                {
                  path: "edit",
                  component: StaffEdit
                }
              ]
            },
            {
              path: `schedule`,
              component: EmptyRoute,
              children: [
                {
                  path: "create",
                  component: ScheduleBuilder,
                  beforeEnter(to, from, next) {
                    if (store.state.user.role === `Program Head`) {
                      next();
                    } else {
                      router.replace(`/${store.state.userGroup}/home`);
                    }
                  }
                },
                {
                  path: "result",
                  component: ScheduleResult,
                  beforeEnter(to, from, next) {
                    if (store.state.user.role === `Program Head`) {
                      next();
                    } else {
                      router.replace(`/${store.state.userGroup}/home`);
                    }
                  }
                }
              ]
            },
            ...routes
          ]
        }
      ]
    },
    {
      path: `*`,
      component: page404
    }
  ]
});

// router.beforeEach(async (to, from, next) => {
//   // await store.commit(`refreshState`);
//   console.log(store.state.isLoggedIn);
//   next();
// });

// router.beforeEach(async (to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     Vue.axios
//       .get(`${store.state.url}/${store.state.userGroup}/keepalive`, {
//         withCredentials: true
//       })
//       .then(() => {
//         next();
//       })
//       .catch(err => {
//         console.log(err);
//         store.dispatch(`logout`);
//         next(`/`);
//       });
//   } else {
//     next();
//   }
// });

export default router;
