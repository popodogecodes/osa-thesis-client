import Vue from "vue";
import Vuex from "vuex";
// import createPersistedState from "vuex-persistedstate";
import VuexPersistence from "vuex-persist";
import createMutationsSharer from "vuex-shared-mutations";
// import cookies from "js-cookie";
const url = require(`../env.config`);

const vuexLocal = new VuexPersistence();

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    sideNavActive: false,
    user: null,
    isLoggedIn: false,
    userGroup: null,
    url,
    about: {
      "Phone Number": null,
      Website: null,
      Address: null
    },
    schedule: {
      startTime: null,
      endTime: null,
      sections: [],
      professors: [],
      rooms: [],
      payload: {}
    }
  },
  getters: {
    getUser: state => state.user
  },
  mutations: {
    updateInfo(state, data) {
      state.about["Phone Number"] = data.phoneNumber;
      state.about.Website = data.website;
      state.about.Address = data.address;
    },

    login(state, data) {
      state.user = data.user;
      state.userGroup = data.userGroup;
      state.isLoggedIn = true;
      state.sideNavActive = false;
    },
    activateSideNav(state, data) {
      state.sideNavActive = data;
    },
    logout(state) {
      state.user = undefined;
      state.userGroup = undefined;
      state.isLoggedIn = false;
      state.sideNavActive = false;
    },
    refreshState() {
      return true;
    },

    pushScheduleTime(state, time) {
      state.schedule.startTime = time.startTime;
      state.schedule.endTime = time.endTime;
    },

    pushToSections(state, section) {
      state.schedule.sections.push(section);
    },

    pushToRooms(state, room) {
      state.schedule.rooms.push(room);
    },

    pushToProfessors(state, professor) {
      state.schedule.professors.push(professor);
    },

    populatePayload(state, payload) {
      state.schedule.payload = payload;
    },

    resetSchedule(state) {
      state.schedule = {
        startTime: ``,
        endTime: ``,
        sections: [],
        professors: [],
        rooms: [],
        payload: null
      };
    }
  },
  actions: {
    login({ commit }, payload) {
      commit(`login`, payload);
    },
    logout({ commit }) {
      commit(`logout`);
    }
  },
  plugins: [
    createMutationsSharer({
      predicate: mutation => {
        const predicate = [
          `login`,
          `activateSideNav`,
          `logout`,
          `refreshState`
        ];
        return predicate.indexOf(mutation.type) >= 0;
      }
    }),
    vuexLocal.plugin
  ]
});
